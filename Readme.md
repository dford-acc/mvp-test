See IRCC HIP Microserves Documentation.doc

 

 

 

 

 

Introduction 

This document details the microservice that was developed by the Accenture team to simulate the HIP integration for Cumulus and GCMS. This code is sample code that was used to validate the event driven design and integration components used in the solution. The goal is that most of the code written in this sample service can be reused for the actual implementation of the HIP Family Class microservice.  

The code itself will be provided in a ZIP file  

 

 

How it Works 

Recall that below is the signed off architecture for the Cumulus-GCMS integration. It involves the Cumulus application, the HIP integration layer, and the legacy GCMS/Siebel components. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Demo Microservice 

The Demo microservice simulates the above architecture all from within the one application. The microservice is broken into 3 components 

Mock Cumulus 

Contains a REST API that takes in either a Decision Payload or an Application Notes Payload, and then publishes that payload to the Kafka Topic. It also listens to the Kafka Confirmation Topic and will report when a confirmation is received back. The REST API and Kafka integration is all done through Red Hat Fuse Routes.  

HIP 

This is the main component of the demo microservice which will be almost entirely reusable for the actual Family Class Microservice. It contains Fuse routes that do the following: 

Listens to the Kafka Decision Topic for new messages 

Listens to the Kafka Application Notes Topic for new messages 

Writes a record to a MongoDB table for each decision/application notes received 

Sends the decision as a JMS message to a MQ queue 

Listens to Confirmation MQ queue and receives the conformation response from GCMS 

Sends the confirmation to the Kafka Confirmation Topic 

 

Mock GCMS 

Contains a Fuse Route that listens to the Decision MQ queue and then creates a Confirmation and puts it on the Confirmation queue.  

 

For simplicity and readability, the Fuse routes were broken up in the code to match the above 3 components: 

 

Each route also has an ID that is a number, which corresponds to the order it is run in the above flow. 

 

External Supporting Components 

On top of the above demo microservice, the following components were deployed onto our OpenShift Cluster to support the Family Class end to end simulation 

 

Apache Kafka (AMQ Streams) 

A Kafka cluster was deployed through the AMQ Streams operator. The microservice sends messages to this Kafka cluster for the decision and confirmation topics. All the supporting resources needed to deploy Kafka on OpenShift has been uploaded to https://alm-tfs.apps.ci.gc.ca/tfs/IRCC/Scrum/_git/dpm-hip-cluster-config?path=%2Fcomponents%2Fapp%2Foperators%2Famq-streams%2Fbase&version=GBmaster  

 

Schema Registry (Red Hat Service Registry) 

A Confluent Schema Registry was deployed to store the schemas for the different payloads (decisions, confirmations, application notes). The microservice would call out to this registry to ensure the schema for the payload was valid. All the supporting resources needed to deploy a Schema Registry on OpenShift has been uploaded to https://alm-tfs.apps.ci.gc.ca/tfs/IRCC/Scrum/_git/dpm-hip-cluster-config?path=%2Fcomponents%2Fapp%2Foperators%2Fservice-registry%2Fbase&version=GBmaster 

 

MongoDB  

A MongoDB instance was deployed to the OpenShift cluster for saving the transactional data. The microservice would save/update a message for each message it received. All the supporting resources needed to deploy Mongo on OpenShift has been uploaded to https://alm-tfs.apps.ci.gc.ca/tfs/IRCC/Scrum/_git/dpm-hip-mvp-configurations?path=%2FFamily%20Class%2FMongoDB 

 

IBM MQ Instance 

An MQ image was deployed onto the OpenShift cluster so that we could create mock MQ queues that represented the GCMS queues. This was done using the following image: https://hub.docker.com/r/ibmcom/mq/  

 

 

How to use it 

Before deploying this in a new cluster, you will have to setup the mentioned external components, and update the microservice to route to those components (For example, update the Kafka connection URL with your new Kafka instance) 

 

Once the microservice and the supporting components are deployed and configured in your cluster, you can test by Posting a sample Decision payload like below:  

 

 

 

Likewise, you can test posting an Application Note by posting a sample notes payload like below: 

 

 

You can then look at the logs in the pod the microservice is deployed in and should see something like below: 

 

 

 

 

 

 

 

 

 

You can also query the Mongo table and Kafka topics to ensure you see the records there 
