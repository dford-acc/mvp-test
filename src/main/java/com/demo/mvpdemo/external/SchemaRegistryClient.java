package com.demo.mvpdemo.external;

import lombok.AllArgsConstructor;
import org.apache.avro.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class SchemaRegistryClient {

    private static final Logger logger = LogManager.getLogger(SchemaRegistryClient.class);

    private final RestTemplate restTemplate;

    public boolean isArtifactSchemaValid(String artifactName, Schema schema) {
        HttpHeaders headers = buildHttpHeaders();
        String schemaUrl = "http://172.30.8.75:8080/api/artifacts/"+artifactName+"/test";
        HttpEntity<String> httpEntity = new HttpEntity<>(schema.toString(), headers);
        logger.info("Cumulus: Validating " + artifactName + " schema against schema registry");
        ResponseEntity<String> response = restTemplate.exchange(schemaUrl, HttpMethod.PUT, httpEntity, String.class);
        return response.getStatusCode().is2xxSuccessful();
    }

    private HttpHeaders buildHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
        headers.add("Accept", "*/*");
        return headers;
    }
}
