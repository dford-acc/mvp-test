package com.demo.mvpdemo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MvpDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvpDemoApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public MongoClient mongoClient() {
		MongoClient test = new MongoClient(new MongoClientURI("mongodb://admin:password@172.30.211.250:27017/"));
		return new MongoClient(new MongoClientURI("mongodb://admin:password@172.30.211.250:27017/"));
	}
}
