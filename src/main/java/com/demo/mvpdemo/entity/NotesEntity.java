package com.demo.mvpdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotesEntity {

    private String correlationId;
    private String applicationNumber;
    private String applicationCategory;
    private String office;
    private String label;
    private String text;
    private Boolean restricted;
    private String activityNumber;

}
