package com.demo.mvpdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DecisionEntity {

    private String correlationId;
    private String applicationNumber;
    private String applicationCategory;
    private String uci;
    private String statusUpdatedBy;
    private String activityNumber;

}
