package com.demo.mvpdemo.routes.gcms;

import com.hip.integration.familyclass.ApplicationDecision;
import com.hip.integration.familyclass.ApplicationNotes;
import com.hip.integration.familyclass.Confirmation;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;


@Component
public class GcmsJmsRoutes extends RouteBuilder {

    @Override
    public void configure() {

        from("jms:queue:HIPMVPGCMS")
                .id("6")
                .bean(this, "createConfirmation")
                .log("GCMS: Picked up decision from Queue")
                .log("GCMS: Processing...")
                .log("GCMS: Decision updated successfully. Sending confirmation to response queue")
                .to("jms:queue:RESPONSEQUEUE?requestTimeout=4500");


    }

    public void createConfirmation(Exchange exchange) {
        Message in = exchange.getIn();
        Confirmation confirmation;
        if (in.getBody(ApplicationDecision.class) != null) {
            ApplicationDecision decision = in.getBody(ApplicationDecision.class);
            confirmation = new Confirmation(decision.getActivityNumber(), "Decision saved to GCMS");
        }
        else {
            ApplicationNotes notes = in.getBody(ApplicationNotes.class);
            confirmation = new Confirmation(notes.getActivityNumber(), "Decision saved to GCMS");
        }
        in.setBody(confirmation);
    }

}

