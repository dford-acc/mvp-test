package com.demo.mvpdemo.routes.hip;

import org.apache.camel.builder.RouteBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class KafkaRoutes extends RouteBuilder {

    private static final Logger logger = LogManager.getLogger(JMSRoutes.class);

    @Override
    public void configure() {

        from("kafka:decision-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092")
                .id("3")
                .unmarshal().avro("com.hip.integration.familyclass.ApplicationDecision")
                .log("HIP: Received decision from Cumulus ${body} with CorrelationID: ${headers[kafka.KEY]}")
                .multicast()
                .to("direct:saveDecision")
                .to("direct:write-to-mq");

        from("kafka:application-notes-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092")
                .id("12")
                .unmarshal().avro("com.hip.integration.familyclass.ApplicationNotes")
                .log("HIP: Received application Notes from Cumulus ${body} with CorrelationID: ${headers[kafka.KEY]}")
                .multicast()
                .to("direct:saveNotes")
                .to("direct:write-to-mq");

        from("direct:publish-confirmation-to-kafka")
                .id("8")
                .id("confirmation-route")
                .log("HIP: Sending confirmation back to Kafka")
                .to("direct:getDecision");
        //.marshal().avro("com.hip.integration.familyclass.Confirmation")
                //.to("kafka:confirmation-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092&bridgeEndpoint=true");

    }
}
