package com.demo.mvpdemo.routes.hip;

import com.demo.mvpdemo.entity.DecisionEntity;
import com.demo.mvpdemo.entity.NotesEntity;
import com.hip.integration.familyclass.ApplicationDecision;
import com.hip.integration.familyclass.ApplicationNotes;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class MongoDbRoutes extends RouteBuilder {

        @Override
        public void configure() {

            from("direct:saveDecision")
                    .id("4")
                    .bean(this, "adaptToDecisionEntity")
                    .log("HIP: Saving Transactional Data to MongoDB")
                    .to("mongodb:mongoClient?database=demodb&collection=test&operation=insert");

            from("direct:saveNotes")
                    .id("13")
                    .bean(this, "adaptToNotesEntity")
                    .log("HIP: Saving Transactional Data to MongoDB")
                    .to("mongodb:mongoClient?database=demodb&collection=test&operation=insert");

            from("direct:getDecision")
                    .id("14")
                    .setBody().constant("{ \"correlationId\": \"${headers.JMSCorrelationID}\" }")
                    .log("HIP: Fetching record")
                    .to("mongodb:mongoClient?database=demodb&collection=test&operation=findOneByQuery")
                    .log("HIP: Got record ${body}");

        }

    public void adaptToDecisionEntity(Exchange exchange) {
        Message in = exchange.getIn();
        ApplicationDecision decision = in.getBody(ApplicationDecision.class);
        in.setBody(new DecisionEntity(in.getHeader("kafka.KEY", String.class),
                   decision.getApplicationNumber(),
                   decision.getApplicationCategory(),
                   decision.getUci(),
                   decision.getStatusUpdatedBy(),
                   decision.getActivityNumber()));
    }

    public void adaptToNotesEntity(Exchange exchange) {
        Message in = exchange.getIn();
        ApplicationNotes notes = in.getBody(ApplicationNotes.class);
        in.setBody(new NotesEntity(in.getHeader("kafka.KEY", String.class),
                notes.getApplicationNumber(),
                notes.getApplicationCategory(),
                notes.getOffice(),
                notes.getLabel(),
                notes.getText(),
                notes.getRestricted(),
                notes.getActivityNumber()));
    }
}
