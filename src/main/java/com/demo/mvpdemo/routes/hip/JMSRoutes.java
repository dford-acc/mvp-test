package com.demo.mvpdemo.routes.hip;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;


@Component
public class JMSRoutes extends RouteBuilder {

    @Override
    public void configure() {

        from("direct:write-to-mq")
                .id("5")
                .setHeader("JMSCorrelationID", simple("${headers[kafka.KEY]}"))
                .setHeader("JMSDeliveryMode", simple("Persistent"))
                .log("HIP: Sending Decision to MQ")
                .to("jms:queue:HIPMVPGCMS?requestTimeout=4500");

        from("jms:queue:RESPONSEQUEUE")
                .id("7")
                .log("HIP: Received Decision Confirmation from MQ: ${body} with CorrelationID: ${headers.JMSCorrelationID}")
                .to("direct:publish-confirmation-to-kafka");

    }

}

