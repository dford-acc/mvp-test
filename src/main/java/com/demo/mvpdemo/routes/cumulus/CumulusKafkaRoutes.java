package com.demo.mvpdemo.routes.cumulus;

import com.demo.mvpdemo.external.SchemaRegistryClient;
import com.demo.mvpdemo.routes.hip.JMSRoutes;
import com.hip.integration.familyclass.ApplicationDecision;
import com.hip.integration.familyclass.ApplicationNotes;
import com.hip.integration.familyclass.Confirmation;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CumulusKafkaRoutes extends RouteBuilder {

    @Autowired
    private SchemaRegistryClient schemaRegistryClient;

    private static final Logger logger = LogManager.getLogger(JMSRoutes.class);

    @Override
    public void configure() {

        from("direct:publish-decision-to-hip")
                .id("2")
                .setHeader(KafkaConstants.KEY, simple("${random(100000, 5000000)}", String.class))
                .log("Cumulus: Received Decision with CorrelationID: ${headers[kafka.KEY]}")
                .bean(this,"validateDecisionSchema")
                .log("Cumulus: Sending the following decision payload to HIP: ${body}")
                .marshal().avro("com.hip.integration.familyclass.ApplicationDecision")
                .to("kafka:decision-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(202))
                .setBody(constant("Decision sent to GCMS!"));

        from("direct:publish-notes-to-hip")
                .id("11")
                .setHeader(KafkaConstants.KEY, simple("${random(100000, 5000000)}", String.class))
                .log("Cumulus: Received Application Notes with CorrelationID: ${headers[kafka.KEY]}")
                .bean(this,"validateNotesSchema")
                .log("Cumulus: Sending the following application notes payload to HIP: ${body}")
                .marshal().avro("com.hip.integration.familyclass.ApplicationNotes")
                .to("kafka:application-notes-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(202))
                .setBody(constant("Application Notes sent to GCMS!"));

        from("kafka:confirmation-topic?brokers=acn-kafka-cluster2-kafka-bootstrap.acn-openshift-amq-streams.svc:9092")
                .id("9")
                .id("cumulus-from-kafka-route")
                .unmarshal().avro("com.hip.integration.familyclass.Confirmation")
                .bean(this,"validateConfirmationSchema")
                .log("Cumulus: Received conformation back from HIP ${body}");

    }


    public void validateDecisionSchema(Exchange exchange) {
        Message in = exchange.getIn();
        ApplicationDecision decision = in.getBody(ApplicationDecision.class);
        if (!schemaRegistryClient.isArtifactSchemaValid("decision-topic", decision.getSchema())) {
            throw new IllegalArgumentException("Cumulus: Decision Schema is not compatible with schema found on the Schema Registry");
        }
        else {
            logger.info("Cumulus: Schema validated!");
        }
    }

    public void validateNotesSchema(Exchange exchange) {
        Message in = exchange.getIn();
        ApplicationNotes notes = in.getBody(ApplicationNotes.class);
        if (!schemaRegistryClient.isArtifactSchemaValid("application-notes-topic", notes.getSchema())) {
            throw new IllegalArgumentException("Cumulus: Application Notes Schema is not compatible with schema found on the Schema Registry");
        }
        else {
            logger.info("Cumulus: Schema validated!");
        }
    }

    public void validateConfirmationSchema(Exchange exchange) {
        Message in = exchange.getIn();
        Confirmation confirmation = in.getBody(Confirmation.class);
        if (!schemaRegistryClient.isArtifactSchemaValid("confirmation-topic", confirmation.getSchema())) {
            throw new IllegalArgumentException("Cumulus: Confirmation Schema is not compatible with schema found on the Schema Registry");
        }
        else {
            logger.info("Cumulus: Schema validated!");
        }
    }

}
