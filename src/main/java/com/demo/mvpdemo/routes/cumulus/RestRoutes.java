package com.demo.mvpdemo.routes.cumulus;

import com.hip.integration.familyclass.ApplicationDecision;
import com.hip.integration.familyclass.ApplicationNotes;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class RestRoutes extends RouteBuilder {

    @Override
    public void configure() {
        restConfiguration().component("servlet").bindingMode(RestBindingMode.json);

        rest().get("/hello-world").produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(constant("Hello World From the MVP Service 2.0!"));

        rest().post("publish-decision")
                .id("1")
                .type(ApplicationDecision.class)
                .to("direct:publish-decision-to-hip");

        rest().post("publish-notes")
                .id("10")
                .type(ApplicationNotes.class)
                .to("direct:publish-notes-to-hip");

    }


}
